const express = require("express");
const service = require("../../services/auth")
const services = require("../../services/multer")
const router = express.Router();
const controller = require("../controllers/index")



router.post("/addrecipe", service.Middleware, controller.recipeController.postRecipe);
router.post("/uploadfile",services.upload.single('image') ,controller.recipeController.fileUpload)
router.get("/getrecipe", controller.recipeController.getRecipe);
router.put("/updaterecipe/:id",service.Middleware, controller.recipeController.updateRecipe);
router.delete("/deleterecipe/:id",service.Middleware, controller.recipeController.deleteRecipe);
module.exports = router