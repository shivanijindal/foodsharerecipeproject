const express = require("express");
const userRoutes = require("./user");
const recipeRoutes = require("./recipe")
const router = express.Router();

router.use("/user", userRoutes)
router.use("/recipe", recipeRoutes)
module.exports = router