const express = require("express");
const router = express.Router();
const controller = require("../controllers/index")



router.post("/sendotp", controller.userController.sendotp);
router.get("/login", controller.userController.loginUser);
router.get("/verify", controller.userController.verifyotp);
router.post("/profile", controller.userController.updateProfile);
router.post("/resetpass", controller.userController.resetPassword);
router.post("/sendresetotp", controller.userController.resetOtp);
router.post("/verifyotp", controller.userController.resetOtpVerify);
router.post("/changepass", controller.userController.changePassword);
module.exports = router