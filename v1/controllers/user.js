require('dotenv').config();
const otpGenerator = require("otp-generator");
const services = require("../../services/index");
const model = require("../../models/index");
const jwt = require("jsonwebtoken");
const user = require('../../models/user');

/*--------------SEND OTP-------------*/
async function sendotp(req, res, next) {
    console.log(req.body)
    const user = await services.userServices.sendotp({ phone: req.body.phone })
    if (user)
        return res.status(400).send("user already");
    const OTP = otpGenerator.generate(6, { digits: true, lowerCaseAlphabets: false, upperCaseAlphabets: false, specialChars: false });
    console.log(OTP);
    const number = req.body.phone;
    const otp = new model.otpModel({ phone: number, otp: OTP });
    const result = await otp.save();
    res.status(200).send("OTP sent successfully");
}


/*-----------VERIFY OTP-------------*/
async function verifyotp(req, res, next) {
    console.log(req.body, '   ', req.body.phone);
    const sentotp = await model.otpModel.findOne({ phone: req.body.phone });

    console.log(sentotp.otp, '===', req.body.otp);
    if (sentotp) {
        if (sentotp.otp === req.body.otp) {
            const user = new model.userModel({ phone: req.body.phone })
            const check = await services.userServices.verifyotp({ phone: req.body.phone })
            if (check) {
                res.status(401).send("user already exists");
            }
            else {
                const token = await user.generateToken();
                const registered = await user.save();
                console.log(token);
                res.status(200).send({
                    message: "otp verified",
                    data: user
                });
            }
        }
        else {
            res.status(400).send("Invalid otp")
        }
    }

}

/*------------------UPDATE PROFILE-------------------*/
async function updateProfile(req, res, next) {
    let token = req.headers['x-acceess-token'];
    if (token == undefined) {
        res.status(400).send({ "error": "Token not found" });
    }
    if (token.startsWith('Bearer')) {
        token = token.slice(7, token.length);
    }
    console.log(token, "tokennn")
    const result = jwt.verify(token, process.env.jwtSecret);
    const user = await model.userModel.findOneAndUpdate({ _id: result._id }, {
        $set: {
            name: req.body.name,
            email: req.body.email,
            password: req.body.password,
            phone: req.body.phone,
            lastLogin: new Date
        }
    }, { new: true });
    console.log(user, 'userrrr')
    res.send(user)
}

/*---------------------RESET SEND OTP----------------------*/
async function resetOtp(req, res, next) {
    try {
        const send=await model.otpModel.deleteMany();
        const data = await services.userServices.resetOtp({ phone: req.body.phone })
        if (data) {
            const OTP = otpGenerator.generate(6, { digits: true, lowerCaseAlphabets: false, upperCaseAlphabets: false, specialChars: false });
            console.log(OTP)
            const number = req.body.phone
            const otp = new model.otpModel({ phone: number, otp: OTP })
            const result = await otp.save()
            res.send(otp)
        } else {
            res.send("number does not exist")
        }
    } catch (error) {
        res.send("something went wrong")
    }
}

/*------------------RESET OTP VERIFY-------------------*/
async function resetOtpVerify(req, res, next) {
    console.log(req.body, '=== ', req.body.phone);
    const sentotp = await model.otpModel.findOne({ phone: req.body.phone });

    console.log(sentotp.otp, '===', req.body.otp);
    if (sentotp) {
        if (sentotp.otp === req.body.otp) {

            const check = await services.userServices.resetOtpVerify({ phone: req.body.phone })
            const token = await check.generateToken();
            const registered = await check.save();
            console.log(token);
            res.status(200).send({
                message: "otp verified",
                data: check
            });

        }
        else {
            res.status(400).send("Invalid otp")
        }
    }
}

/*-----------------RESET PASSWORD---------------- */
async function resetPassword(req, res, next) {
    let token = req.headers['x-acceess-token'];
    if (token == undefined) {
        res.status(400).send({ "error": "Token not found" });
    }
    if (token.startsWith('Bearer')) {
        token = token.slice(7, token.length);
    }
    console.log(token, "tokennn")
    const result = jwt.verify(token, process.env.jwtSecret);
    const user = await model.userModel.findOneAndUpdate({ _id: result._id }, {
        $set: {
            password: req.body.password
        }
    }, { new: true });
    console.log(user, 'userr')
    res.send(user)
}


/*-------------CHANGE PASSWORD-------------*/
async function changePassword(req, res, next) {
    try {
        const user = await model.userModel.findOne({ phone: req.body.phone })
        console.log(user);
        if (user) {
            console.log("aaaa")
            console.log(user.password, '===', req.body.oldPassword)
            if (user.password === req.body.oldPassword) {
                const data = await model.userModel.findOneAndUpdate({ phone: req.body.phone }, {
                    $set: {
                        password: req.body.password
                    }
                }, { new: true })
                const result = data.save()
                console.log("aaaaaaasdfg")
            } else {
                res.send("Old password is not matching")
            }
        } else {
            res.send("user does not exist")
        }
        res.status(200).send({
            message: "password updated successfully"
        })
    } catch (error) {
        console.log(error)
        res.send(error)
    }
}

/*--------------------LOGIN USER---------------------*/
async function loginUser(req, res) {
    try {
        const email = req.body.email
        const password = req.body.password
        const user = await model.userModel.findOne({ email: email })
        console.log(user)
        if (!user) {
            res.send("email is not registered")
        }
        else if (user.password != password)
            res.send("wrong password")

        if (user.email === req.body.email && user.password === req.body.password) {
            console.log(user)
            res.status(200).send({
                message: "log in successfully",
                data: user
            })
        } else {
            res.send("details are not present")
        }
    } catch (error) {
        res.status(400).send({
            message: "Invalid details"
        })
    }
}


module.exports = {
    sendotp,
    verifyotp,
    updateProfile,
    resetOtp,
    resetOtpVerify,
    resetPassword,
    changePassword,
    loginUser
}