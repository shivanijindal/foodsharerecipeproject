const model = require("../../models/index");
const services = require("../../services/index")
async function postRecipe(req, res, next) {
    try {
        req.body.userId=req.user._id;
        const recipe = await model.recipeModel.create(req.body);
        if (recipe) {
            return res.status(201).send({
                message: "Recipe added successfully",
                data: recipe

            })
        }
        return res.status(500).send({
            message: "Something went wrong",

        })
    } catch (error) {
        next(error)
    }
}
async function fileUpload(req, res, next) {
    try {
        console.log(req.file.filename,"ooooooo")
        let data = await services.recipeServices.fileUpload(req, req.recipe);
        return res.status(200).send({
            message:"uploaded",
            data:req.file.filename 

        });
    } catch (error) {
        next(error);
    }
}

async function getRecipe(req, res, next) {
    try {
        const recipe = await model.recipeModel.find();
        if (!recipe) {
            return res.status(404).send({
                message: "Recipe is not available",
                data: {}
            });
        } else {
            return res.status(201).send({
                message: "recipe found",
                data: recipe
            })
        }
    } catch (error) {
        next(error)
    }
}

async function updateRecipe(req, res) {
    console.log(req.query.id)
    const recipe = await model.recipeModel.findByIdAndUpdate(req.params.id, { $set: req.body })
    if (!recipe) {
        console.log("invalid data")
    } else {
        res.send(recipe)
    }
}

async function deleteRecipe(req, res) {
    console.log(req.query.id)
    const recipe = await model.recipeModel.findByIdAndDelete(req.params.id, { $set: req.body })
    if (!recipe) {
        console.log("invalid data")
    } else {
        res.send(recipe)
    }
}


module.exports = {
    postRecipe,
    getRecipe,
    updateRecipe,
    deleteRecipe,
    fileUpload
}