I've developed a food recipes Share application.
I've used NODEJS, EXPRESS, MONGODB for database
There are two models in this one is for user and other for recipes.
API's that I've created for user are 
1.)signup for which user will enter his number then he'll get an otp after that he will get a page for verify
2.)After verify he'll get a page for profile updation where he'll update all his details as asked
3.)If the user want to change their password then there is a api where user have to enter phone no., old password and a new password so that user can change the password.
4.)If in case a user forgot his password then he can reset his password for that he'll get an otp and after otp verification user can reset his password.
5.)By filling the email and password details user can easily login

API's that I've created for recipes are i.e CRUD
1.)A create api where user can add a recipe
2.) A Get api from where we can get all recipes uploaded
3.) A update api where user can easily update a post created by him
4.) A delete api where user can easily delete a post created by him
