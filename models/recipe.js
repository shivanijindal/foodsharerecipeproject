const mongoose = require("mongoose");
const recipeModel = new mongoose.Schema({
    title: {
        type: String
    },
    description: {
        type: String
    },
    image: {
        type: String
    },
    userId:{
type: mongoose.Schema.ObjectId,
ref:"user"
    },
    createdAt: { type: Date, default: Date.now }

})

const recipe = mongoose.model("recipe", recipeModel);
module.exports = recipe;