const mongoose = require("mongoose");
const otpModel = new mongoose.Schema({
    phone: {
        type: String
    },
    otp: {
        type: String
    },
    expiresAt: { type: Date, default: Date.now, index: { expires: "5min" } }


})

const otp = mongoose.model("otp", otpModel);
module.exports = otp;