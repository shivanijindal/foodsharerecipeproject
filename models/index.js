module.exports = {
    userModel: require("./user"),
    recipeModel: require("./recipe"),
    otpModel: require("./otp")
}