require('dotenv').config();
const services = require("../services/jwt")
const mongoose = require("mongoose");
const userModel = new mongoose.Schema({
    name: {
        type: String
    },
    email: {
        type: String
    },
    password: {
        type: String
    },
    phone: {
        type: String
    },
    jti: {
        type: String
    },
    lastLogin: {
        type: Date,
    },
    //   tokens : [{
    //         token : {
    //             type : String,
    //             required : true
    //         }
    //     }]
    tokens:String

})
userModel.methods.generateToken = async function () {
    try {
        let jti = Math.random().toString(36).slice(2) + Math.random().toString(36).slice(2);
        console.log(this._id);
        const token = services.sign({ _id: this._id.toString() }, { $set: { jti: jti } }, process.env.jwtSecret);
        this.jti = jti;
        await this.save();
        // this.tokens = this.tokens.concat({ token: token });
        this.tokens=token;
        await this.save();
        return token;
    } catch (err) {
        return err;
    }
}


const user = mongoose.model("user", userModel);
module.exports = user;