const express = require("express");
require('dotenv').config();
const app = express();
const conn = require("./conn/db");
const v1Routes = require("./v1/routes/index");
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use("/v1", v1Routes)
app.use("/images",express.static(__dirname+"/images"))

app.use((err, req, res, next) => {
    res.status(400);
    res.send({
        message: err || err.message,
        data: {}
    })
})
app.listen(8080, () => {
    console.log("Server is running at 8000 port");
});