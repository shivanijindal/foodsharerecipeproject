module.exports = {
    jwtServices: require("./jwt"),
    userServices: require("./user"),
    recipeServices: require("./recipe")
}