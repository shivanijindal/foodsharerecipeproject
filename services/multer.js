const express=require("express");
    const multer=require("multer");

    const app=express();


const fileStorageEngine = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './images')
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + '--' + file.originalname)
    }
})

const multerFilter=(req,file,cb)=>{
    if(file.mimetype.split("/")[1]==="png" || file.mimetype.split("/")[1]==="pdf"){
        cb(null, './images')
    } else {
        cb(new Error("Not a valid File"), false);
    }
}

    const upload=multer({storage: fileStorageEngine,
    fileFilter: multerFilter
})

//     app.post("/single", upload.single('image'),(req, res)=>{
//         console.log(req.file);
// res.send("Single File Upload Success");
//     });
module.exports={
    upload
}