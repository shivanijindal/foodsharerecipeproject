require('dotenv').config();
var jwt = require('jsonwebtoken');

function sign(payload) {
    const token = jwt.sign(payload, process.env.jwtSecret);
    console.log(token)
    return token
}
function verify(token) {
    try {
        console.log("verify")
        var decoded = jwt.verify(token, process.env.jwtSecret);
        console.log(decoded,"*********")
        return decoded
    } catch (error) {
        return null
    }
}

module.exports = {
    sign,
    verify
}