
async function fileUpload(req,res) {

    if (!req.file) throw new Error("Error while uploading");
    let filePath = req.file.location
    return filePath

}
module.exports={
    fileUpload
}