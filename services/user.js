const model = require("../models/index");


async function sendotp(body) {
    const data = await model.userModel.findOne(body);
    return data;
}

async function verifyotp(body) {
    const data = await model.userModel.findOne(body)
    return data;
}

async function resetOtp(body) {
    const data = await model.userModel.findOne(body)
    return data;
}

async function resetOtpVerify(body) {
    const data = await model.userModel.findOne(body)
    return data;
}

module.exports = {
    sendotp,
    verifyotp,
    resetOtp,
    resetOtpVerify
}